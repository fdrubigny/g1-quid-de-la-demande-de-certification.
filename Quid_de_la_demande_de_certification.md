# Quid de la demande de certification.

_Je suis Co-créateur de Ğ1 depuis le 10 Septembre 2017._
 
Après plusieurs mois de recherches sur la monnaie libre et tout ce qui gravite autour (monnaie libre et monnaie dette, certification, blockchain...), j'ai rencontré des membres locaux et j'ai tissé des liens avec ces personnes. Cela a été rapide pour moi de devenir certifié (malgré quasiment six mois d'attente), en comparaison à d'autres. A ce propos, l'idée n'est pas tant d'aller vite ou de toucher son DU que de **comprendre les valeurs véhiculées par la June**, notamment que c'est **une expérimentation humaine et technique au service de l'humain et des échanges de quelque sorte qu'ils soient**.

## Voici un rappel de ce que tu dois savoir avant de penser à la certification :

En tant que **membre de la toile de confiance**, pour que je puisse certifier une personne et la faire rentrer dans ladite toile [https://duniter.org/fr/wiki/toile-de-confiance](https://duniter.org/fr/wiki/toile-de-confiance) je dois m'assurer que :

**La personne ai bien lu et compris la licence**, et qu’elle saura l’expliquer et la retransmettre à son tour [https://duniter.org/fr/monnaie-libre-g1/licence-g1](https://duniter.org/fr/monnaie-libre-g1/licence-g1)

**Je connais « suffisamment » la personne** en l’ayant rencontrée plusieurs fois en réel et/ou en visio (cf. Point 4a et 4b de la licence), j'ai échangé avec elle et discuté, je peux également la recontacter par divers moyens (téléphone, email, courrier postal...)

**La personne a compris comment utiliser Césium**, sait distinguer un compte portefeuille d’un compte membre, sait effectuer un virement et a **bien sécurisé son compte** [https://forum.duniter.org/t/recommandations-officielles-de-securite-a-lire-avant-de-creer-votre-compte-g1/1829](https://forum.duniter.org/t/recommandations-officielles-de-securite-a-lire-avant-de-creer-votre-compte-g1/1829) (**notamment qu'elle a réalisé une sauvegarde** *de ses identifiants à plusieurs endroits - le couple identifiant et mot de passe est personnel et privé, il définit la clé publique et ne peut pas être retrouvé - et de son fichier de révocation*) [https://cesium.app/fr/tutoriel-cesium/creer-un-compte/compte-membre/creer-fichier-de-revocation](https://cesium.app/fr/tutoriel-cesium/creer-un-compte/compte-membre/creer-fichier-de-revocation)

La personne a compris que **le Dividende Universel (DU) n’est pas un revenu de base mais une participation à la Co-création de la masse monétaire Ğ1**.

**La personne ne cherchera pas à se faire certifier plusieurs comptes membres lui appartenant**, car _un seul compte membre, Co-créateur de la monnaie par être humain, est indiqué dans la licence_. ***_Ce compte membre certifie une personne physique, une identité réelle et unique._***

**La personne a compris les bases de la monnaie libre et de sa première devise Ğ1 d’une manière globale** et cherchera donc en premier lieu à **rencontrer du monde** afin de pouvoir faire des échanges tant humains qu'économiques.

**La personne saura retransmettre l’ensemble de ces informations de base** qui demandent à être approfondies régulièrement.

Pour pouvoir être certifié·e, **il faut absolument rencontrer du monde, participer à la vie de la June, échanger, proposer des services…** Des sites spécialisés existent comme :

* Gchange [https://www.gchange.fr/#/app/wallet](https://www.gchange.fr/#/app/wallet)
* Ğ1 Bien [https://g1bien.com](https://g1bien.com) 
* Airbnjune [https://airbnjune.org/](https://airbnjune.org/)
* G1formation [https://g1formation.fr](https://g1formation.fr)

**_La Co-création monétaire par le DU n’est pas le « Graal »_**, c'est une étape importante certes, mais il ne sert à rien de courir après. **Les certifications viennent à toi naturellement à partir du moment où tu es actif·ve dans la communauté**. A force d’échanger et de rencontrer des membres, d’eux-mêmes au bout d’un moment, ils proposeront de te certifier. Note d'ailleurs que _tu dois connaître la mécanique de certification_ (délai entre deux certifications émises, pas, qualité) pour mieux appréhender ce potentiel, qui deviendra, quand tu seras membre, ta responsabilité.

On gagne bien plus de Junes en proposant des produits et services à la communauté qu’en coproduisant le DU qui, infine, ne représente à l’heure actuelle qu'un peu plus de 300 Ğ1 par mois… ce qui est très peu comparé aux montants échangés sur les produits et services. D'autre part, **Co-produire de la masse monétaire à outrance et ne pas s’en servir est une aberration, la circulation de la monnaie étant l'huile dans les rouages de la Ğ1**.

Pour avoir une meilleure compréhension de l’ensemble et faire tes armes, je t'invite dans un premier temps à participer aux visios qui se déroulent quasiment toute la semaine [https://forum.monnaie-libre.fr/t/organisation-dune-chaine-visios/13044](https://forum.monnaie-libre.fr/t/organisation-dune-chaine-visios/13044) Même si certaines sont indiquées pour un secteur géographique donné, il n’en n’est rien : __tout le monde est bienvenue à toutes les visios__, **la priorité étant toujours donnée aux nouveaux venus** afin de répondre à leurs nombreuses (et légitimes) interrogations.

Comprends-moi bien, mon message n’est pas là pour te décourager ou te faire peur. Notre intérêt commun est de **t'aiguiller correctement vers les « bonnes façons » de procéder**. _Tu as également un devoir de compréhension de la monnaie (dette et libre) et de recherche d'informations_. **Mais sache que si tu t'intéresses à la June, tu recevras tous ses bienfaits.**

### Merci d'avance de ton temps et de ton implication pour changer les regards sur la monnaie et, par là même, sur le monde et l'humanité.

« *Ce contenu est mis à disposition selon les termes de la Licence Creative Commons Attribution 4.0 International* [https://creativecommons.org/licenses/by/4.0/](https://creativecommons.org/licenses/by/4.0/) » 

[Parhit](https://parhit.fr/) et [Francis Drubigny](https://f.drubigny.fr) 
